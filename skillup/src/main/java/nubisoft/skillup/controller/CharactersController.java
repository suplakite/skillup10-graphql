package nubisoft.skillup.controller;

import nubisoft.skillup.model.character.Character;
import nubisoft.skillup.model.character.Episode;
import nubisoft.skillup.service.CharactersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CharactersController {

    CharactersService charactersService;

    @Autowired
    public CharactersController(CharactersService charactersService) {
        this.charactersService = charactersService;
    }

    @QueryMapping
    List<Character> characters() {
        return charactersService.getCharactersFromEpisode();
    }

}
