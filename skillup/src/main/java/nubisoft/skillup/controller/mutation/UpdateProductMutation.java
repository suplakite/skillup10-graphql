package nubisoft.skillup.controller.mutation;

public record UpdateProductMutation(Integer id, String name, Integer price) {
}
