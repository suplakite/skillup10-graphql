package nubisoft.skillup.controller.mutation;

import nubisoft.skillup.controller.mutation.input.ProductTypeInput;
import nubisoft.skillup.model.product.ProductType;

import java.util.List;
import java.util.stream.Collectors;

public record CreateProductMutation(String name, Integer price, List<ProductTypeInput> availableTypes) {
    public List<ProductType> getProductTypes() {
        return availableTypes.stream().map(input -> new ProductType(input.size(), input.color(), input.sex())).collect(Collectors.toList());
    }
}

