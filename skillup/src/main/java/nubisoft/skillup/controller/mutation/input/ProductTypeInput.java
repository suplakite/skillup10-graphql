package nubisoft.skillup.controller.mutation.input;

import nubisoft.skillup.model.product.ColorType;
import nubisoft.skillup.model.product.SexType;
import nubisoft.skillup.model.product.SizeType;


public record ProductTypeInput(SizeType size, ColorType color, SexType sex) {

}