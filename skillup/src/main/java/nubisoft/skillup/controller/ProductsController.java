package nubisoft.skillup.controller;

import nubisoft.skillup.controller.mutation.CreateProductMutation;
import nubisoft.skillup.controller.mutation.UpdateProductMutation;
import nubisoft.skillup.model.product.Product;
import nubisoft.skillup.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.*;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductsController {

    private final ProductsService productsService;

    @Autowired
    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @SchemaMapping(typeName = "Query", value = "products")
    List<Product> products() {
        return productsService.getProducts();
    }

    @QueryMapping
    Product product(@Argument Integer id) {
        return productsService.getProductById(id);
    }

    @QueryMapping
    List<Product> productsByPrice(@Argument Integer price, @Argument Integer priceTo) {
        return productsService.getProducts().stream().filter(product -> product.getPrice() > price && product.getPrice() < priceTo).collect(Collectors.toList());
    }

    @MutationMapping
    Integer createProduct(@Argument CreateProductMutation mutation) {
        System.out.println("Start1");
        var productsService1 = productsService.createProduct(mutation);
        System.out.println("Finish1");
        return productsService1;

    }

    @MutationMapping
    Integer updateProduct(@Argument UpdateProductMutation mutation) {
        System.out.println("Start2");
        var xd = productsService.updateProduct(mutation);
        System.out.println("Finish2");
        return xd;
    }

    @MutationMapping
    Integer deleteProduct(@Argument Integer id) {
        return productsService.deleteProdcut(id);
    }


}
