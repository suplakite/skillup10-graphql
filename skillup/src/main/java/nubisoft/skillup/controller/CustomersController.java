package nubisoft.skillup.controller;

import nubisoft.skillup.model.customer.Account;
import nubisoft.skillup.model.customer.Customer;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class CustomersController {

    @QueryMapping
    List<Customer> customers() {
        return List.of(new Customer(1, "A"),
                new Customer(2, "B"),
                new Customer(3, "C"),
                new Customer(4, "D")
        );
    }

//    @SchemaMapping(typeName = "Customer")
//    Account account(Customer customer) {
//        System.out.println("Getting account for customer: " + customer.id());
//        return new Account(customer.id());
//    }

    @BatchMapping
    Map<Customer, Account> account(List<Customer> customers) {
        System.out.println("Getting account for customers");
        return customers.stream().collect(Collectors.toMap(customer -> customer, customer -> new Account(customer.id())));
    }

}
