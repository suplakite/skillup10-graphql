package nubisoft.skillup.service;

import nubisoft.skillup.controller.mutation.CreateProductMutation;
import nubisoft.skillup.controller.mutation.UpdateProductMutation;
import nubisoft.skillup.model.product.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ProductsService {

    private final List<Product> products = new ArrayList<>();

    private final AtomicInteger productId = new AtomicInteger(0);

    @PostConstruct
    void prepareAvailableProducts() {
        List.of("Koszula Dzik", "Koszula Kot", "Koszula Byk", "Koszula Ryś", "Koszula Bizon").forEach(name -> products.add(
                new Product(
                        productId.incrementAndGet(),
                        name,
                        new Random().nextInt(100),
                        List.of(prepareRandomProductType(), prepareRandomProductType(), prepareRandomProductType(), prepareRandomProductType())
                )));
    }

    public List<Product> getProducts() {
        return products;
    }

    public Product getProductById(Integer id) {
        return products.stream().filter(product -> Objects.equals(product.getId(), id)).findFirst().orElse(null);
    }

    public Integer createProduct(CreateProductMutation mutation) {
        products.add(
                new Product(
                        productId.incrementAndGet(),
                        mutation.name(),
                        mutation.price(),
                        mutation.getProductTypes()
                )
        );
        return productId.get();
    }

    public Integer updateProduct(UpdateProductMutation mutation) {
        products.forEach(product -> {
            if (Objects.equals(product.getId(), mutation.id())) {
                product.setName(mutation.name());
                product.setPrice(mutation.price());
            }
        });
        return mutation.id();
    }

    public Integer deleteProdcut(Integer id) {
        products.removeIf(product -> Objects.equals(product.getId(), id));
        return id;
    }

    private ProductType prepareRandomProductType() {
        return new ProductType(
                getRandom(List.of(SizeType.values())),
                getRandom(List.of(ColorType.values())),
                getRandom(List.of(SexType.values()))
        );
    }

    private <T> T getRandom(List<T> list) {
        return list.get(new Random().nextInt(list.size()));
    }
}
