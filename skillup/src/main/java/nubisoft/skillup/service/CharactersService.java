package nubisoft.skillup.service;

import nubisoft.skillup.model.character.Character;
import nubisoft.skillup.model.character.Droid;
import nubisoft.skillup.model.character.Episode;
import nubisoft.skillup.model.character.Human;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharactersService {

    private final List<Character> characters = new ArrayList<>();

    @PostConstruct
    void setupCharacters() {
        characters.add(
                new Human(1, "Skywalker", List.of(Episode.JEDI, Episode.EMPIRE), "Earth")
        );
        characters.add(
                new Human(2, "Skywalker", List.of(Episode.NEWHOPE, Episode.EMPIRE), "Earth")
        );
        characters.add(
                new Droid(3, "ZeegBeeg", List.of(Episode.NEWHOPE, Episode.EMPIRE), "Shoot")
        );
    }

    public List<Character> getCharactersFromEpisode() {
        return characters;
    }
}
