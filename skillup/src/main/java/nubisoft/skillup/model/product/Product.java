package nubisoft.skillup.model.product;

import java.util.List;

public class Product {

    private Integer id;
    private String name;
    private Integer price;
    private List<ProductType> availableTypes;

    public Product(Integer id, String name, Integer price, List<ProductType> availableTypes) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.availableTypes = availableTypes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<ProductType> getAvailableTypes() {
        return availableTypes;
    }

    public void setAvailableTypes(List<ProductType> availableTypes) {
        this.availableTypes = availableTypes;
    }
}
