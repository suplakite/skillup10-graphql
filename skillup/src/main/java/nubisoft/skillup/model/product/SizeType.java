package nubisoft.skillup.model.product;

public enum SizeType {
    S,
    M,
    L,
    XL
}
