package nubisoft.skillup.model.product;

public enum SexType {
    WOMAN,
    MAN,
    UNISEX
}
