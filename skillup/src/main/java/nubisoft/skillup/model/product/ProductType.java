package nubisoft.skillup.model.product;

public record ProductType(SizeType size, ColorType color, SexType sex) {
}
