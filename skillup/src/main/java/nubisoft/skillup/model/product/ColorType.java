package nubisoft.skillup.model.product;

public enum ColorType {
    GREEN,
    RED,
    BLACK,
    WHITE
}
