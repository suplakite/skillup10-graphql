package nubisoft.skillup.model.customer;

public record Customer(Integer id, String name) {
}
