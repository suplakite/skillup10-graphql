package nubisoft.skillup.model.character;

import java.util.List;

public class Human extends Character {

    private final String planet;

    public Human(Integer id, String name, List<Episode> appearsIn, String planet) {
        super(id, name, appearsIn);
        this.planet = planet;
    }
}
