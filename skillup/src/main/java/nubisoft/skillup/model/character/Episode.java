package nubisoft.skillup.model.character;

public enum Episode {
    NEWHOPE,
    EMPIRE,
    JEDI
}
