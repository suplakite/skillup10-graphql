package nubisoft.skillup.model.character;

import java.util.List;

public class Droid extends Character {

    private final String primaryFunction;

    public Droid(Integer id, String name, List<Episode> appearsIn, String primaryFunction) {
        super(id, name, appearsIn);
        this.primaryFunction = primaryFunction;
    }
}
