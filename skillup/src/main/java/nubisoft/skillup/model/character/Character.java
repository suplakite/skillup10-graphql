package nubisoft.skillup.model.character;

import java.util.List;

public abstract class Character {

    private final Integer id;

    private final String name;

    private final List<Episode> appearsIn;

    public Character(Integer id, String name, List<Episode> appearsIn) {
        this.id = id;
        this.name = name;
        this.appearsIn = appearsIn;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Episode> getAppearsIn() {
        return appearsIn;
    }
}
