package nubisoft.skillup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillupApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkillupApplication.class, args);
	}

}
