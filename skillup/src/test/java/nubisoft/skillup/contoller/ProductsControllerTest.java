package nubisoft.skillup.contoller;

import nubisoft.skillup.controller.ProductsController;
import nubisoft.skillup.model.product.Product;
import nubisoft.skillup.service.ProductsService;
import nubisoft.skillup.utils.TestDataGenerator;
import nubisoft.skillup.utils.TestRequestsGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.GraphQlTest;
import org.springframework.context.annotation.Import;
import org.springframework.graphql.test.tester.GraphQlTester;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Import(ProductsService.class)
@GraphQlTest(ProductsController.class)
public class ProductsControllerTest {

    @Autowired
    GraphQlTester graphQlTester;

    @Test
    void must_return_list_of_products() {
        graphQlTester.document(TestRequestsGenerator.prepareGetProductsQuery())
                .execute()
                .path("products")
                .entityList(Product.class)
                .hasSize(5);
    }

    @Test
    void must_return_product_by_id() {
        graphQlTester.document(TestRequestsGenerator.prepareGetProductByIdQuery())
                .variable("id", 1)
                .execute()
                .path("product")
                .entity(Product.class)
                .satisfies(product -> {
                    assert product.getId() == 1;
                    assert product.getName().contains("Koszula Dzik");
                });
    }

    @Test
    void must_create_new_product() {
        Map<String, Object> createProductMutation = TestDataGenerator.prepareCreateProductMutation();

        Integer id = graphQlTester.document(TestRequestsGenerator.prepareCreateProductMutation())
                .variable("mutation", createProductMutation)
                .execute()
                .path("createProduct")
                .entity(Integer.class)
                .get();

        graphQlTester.document(TestRequestsGenerator.prepareGetProductByIdQuery())
                .variable("id", id)
                .execute()
                .path("product")
                .entity(Product.class)
                .satisfies(product -> {
                    assert Objects.equals(product.getId(), id);
                    assert product.getName().contains((String) createProductMutation.get("name"));
                    assert product.getPrice() == createProductMutation.get("price");
                });
    }

    @Test
    void must_update_product() {
        Map<String, Object> createProductMutation = TestDataGenerator.prepareCreateProductMutation();

        Integer id = graphQlTester.document(TestRequestsGenerator.prepareCreateProductMutation())
                .variable("mutation", createProductMutation)
                .execute()
                .path("createProduct")
                .entity(Integer.class)
                .get();

        Map<String, Object> updateProductMutation = TestDataGenerator.prepareUpdateProductMutation(id);

        graphQlTester.document(TestRequestsGenerator.prepareUpdateProductMutation())
                .variable("mutation", updateProductMutation)
                .execute().path("updateProduct")
                .entity(Integer.class);

        graphQlTester.document(TestRequestsGenerator.prepareGetProductByIdQuery())
                .variable("id", id)
                .execute().path("product")
                .entity(Product.class)
                .satisfies(product -> {
                    assert Objects.equals(product.getId(), id);
                    assert product.getName().contains((String) updateProductMutation.get("name"));
                    assert product.getPrice() == updateProductMutation.get("price");
                });
    }

    @Test
    void must_delete_product() {
        Integer id = graphQlTester.document(TestRequestsGenerator.prepareCreateProductMutation())
                .variable("mutation", TestDataGenerator.prepareCreateProductMutation())
                .execute()
                .path("createProduct")
                .entity(Integer.class)
                .get();

        List<Product> productsBeforeDelete = graphQlTester.document(TestRequestsGenerator.prepareGetProductsQuery())
                .execute()
                .path("products")
                .entityList(Product.class)
                .get();

        assert productsBeforeDelete.stream().filter(product -> Objects.equals(product.getId(), id)).findFirst().orElse(null) != null;

        graphQlTester.document(TestRequestsGenerator.prepareDeleteProductMutation())
                .variable("id", id)
                .execute()
                .path("deleteProducts");

        List<Product> productsAfterDelete = graphQlTester.document(TestRequestsGenerator.prepareGetProductsQuery())
                .execute()
                .path("products")
                .entityList(Product.class)
                .get();

        assert productsAfterDelete.stream().filter(product -> Objects.equals(product.getId(), id)).findFirst().orElse(null) == null;
    }

}
