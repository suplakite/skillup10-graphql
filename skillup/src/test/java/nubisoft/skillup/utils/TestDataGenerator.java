package nubisoft.skillup.utils;

import nubisoft.skillup.model.product.ColorType;
import nubisoft.skillup.model.product.SexType;
import nubisoft.skillup.model.product.SizeType;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class TestDataGenerator {

    public static Map<String, Object> prepareCreateProductMutation() {
        return Map.of(
                "name", "Create Product",
                "price", 55,
                "availableTypes", List.of(Map.of("size", SizeType.L, "color", ColorType.GREEN, "sex", SexType.UNISEX))
        );
    }

    public static Map<String, Object> prepareUpdateProductMutation(Integer id) {
        return Map.of(
                "id", id,
                "name", "Update product",
                "price", new Random().nextInt(100)
        );
    }

}
