package nubisoft.skillup.utils;

public class TestRequestsGenerator {

    // to have assistance from IntelliJ in defining graphQl queries it is necessary to create .graphqlconfig file and add comment "//language=GraphQL" above query or mutation

    public static String prepareGetProductsQuery() {
        //language=GraphQL
        return """
                query {
                    products {
                            id
                            name
                            price
                    }                  
                }
                """;
    }

    public static String prepareGetProductByIdQuery() {
        //language=GraphQL
        return """
                query getProductById($id: ID!){
                    product(id: $id) {
                        id
                      	name
                        price
                        availableTypes {
                          size
                          color
                          sex
                        }
                    }
                }
                """;
    }

    public static String prepareCreateProductMutation() {
        //language=GraphQL
        return """
                mutation createProduct($mutation: CreateProductMutation){
                    createProduct(mutation: $mutation)
                }
                """;
    }

    public static String prepareUpdateProductMutation() {
        //language=GraphQL
        return """
                mutation updateProduct($mutation: UpdateProductMutation){
                    updateProduct(mutation: $mutation)
                }
                """;
    }

    public static String prepareDeleteProductMutation() {
        //language=GraphQL
        return """
                mutation deleteProduct($id: ID){
                    deleteProduct(id: $id)
                }
                """;
    }

}
